<?php
/**
 * Created by PhpStorm.
 * User: DK
 * Date: 27.12.2023
 * Time: 2:04
 */

if (!defined('WP_UNINSTALL_PLUGIN')) exit;

delete_option( 'weather_state' );

global $wpdb;
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}weather_plugin" );
