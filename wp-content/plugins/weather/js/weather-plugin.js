let widgetScrollPositionOnTopDefault = 0;
let widgetScrollPositionOnLeftDefault = 0;

$(window).on('load', function (e) {
	if (!$('.weather-plugin').length) {
		return null;
	}

	let pageScroll = $(document).scrollTop();
	widgetScrollPositionOnTopDefault = $('.weather-plugin').offset().top;
	widgetScrollPositionOnLeftDefault = $('.weather-plugin').offset().left;

	if(pageScroll > widgetScrollPositionOnTopDefault){
		if (!$('.weather-plugin').hasClass('fixed')) {
			$('.weather-plugin').addClass('fixed')
				.css("top", widgetScrollPositionOnTopDefault + "px")
				.css("left", widgetScrollPositionOnLeftDefault + "px");
		}
	}

	$(window).on('scroll', function (e) {
		let pageScroll = $(document).scrollTop();

		if(pageScroll < widgetScrollPositionOnTopDefault){
			if ($('.weather-plugin').hasClass('fixed'))
				$('.weather-plugin').removeClass('fixed');
		} else {
			if (!$('.weather-plugin').hasClass('fixed'))
				$('.weather-plugin').addClass('fixed')
					.css("left", widgetScrollPositionOnLeftDefault + "px");
		}
	});
});


