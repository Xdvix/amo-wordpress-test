<?php
/**
 * Created by PhpStorm.
 * User: DK
 * Date: 26.12.2023
 * Time: 2:12
 */

defined('ABSPATH') or die;

add_filter( 'plugin_action_links', 'action_links', 10, 2 );

add_action(
	'admin_menu',
	'admin',
	10, 0
);

function action_links ( $links, $file ) {
	if ( $file != 'weather/weather.php' ) {
		return $links;
	}

	$settings_link = sprintf('<a href="%s">%s</a>', admin_url('admin.php?page=weather-admin'), 'Настройки');
	array_unshift( $links, $settings_link );

	return $links;
}

function admin () {
	if ( function_exists('add_options_page') )
	{
		add_options_page(
			__( 'Настройки weather', 'textdomain' ),
			__( 'Настройки weather', 'textdomain' ),
			'manage_options',
			'weather-admin',
			'admin_form'
		);
	}
}

function admin_form () {
	$plugin_state = filter_var( get_option('weather_state'), FILTER_VALIDATE_BOOLEAN );
	if (isset($_GET['weather-state'])) {
		$state = filter_var( $_GET['weather-state'], FILTER_VALIDATE_BOOLEAN );

		if ($plugin_state !== $state) {
			update_option('weather_state', $state);
			$plugin_state = $state;
		}
	}

	$anchor = 'Включить';
	$href = 'true';

	if ($plugin_state) {
		$anchor = 'Выключить';
		$href = 'false';
	}

	echo '<a href="?page=weather-admin&weather-state=' . $href . '">' . $anchor . '</a>';

	global $wpdb;
	$data = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}weather_plugin" );
	echo '<table border="1">
	<caption>Последние 5 запросов</caption>
	<tr>
		<th>Ид</th>
		<th>Дата</th>
		<th>Город</th>
		<th>Температура</th>
	</tr>
	';
	foreach ($data as $q) {
		echo '
			<tr>
				<td>' . $q->id . '</td>
				<td>' . $q->date_time . '</td>
				<td>' . $q->city . '</td>
				<td>' . $q->temp . '</td>
			</tr>
		';
	}
	echo '</table>';
}
