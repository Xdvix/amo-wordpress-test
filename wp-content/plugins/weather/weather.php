<?php
/**
 * Created by PhpStorm.
 * User: DK
 * Date: 26.12.2023
 * Time: 0:08
 * Plugin Name:        Weather
 * Description:        Плавающий погодный информер
 * Plugin URI:         /----------/
 * Author URI:         /----------/
 * Author:             Дмитрий Кулагин
 * Version:            1.0
 *
 * Text Domain:        weather
 * Domain Path:        /----------/
 * Requires at least:  2.5
 * Requires PHP:       7.4
 *
 * License:            GPL2
 * License URI:        https://www.gnu.org/licenses/gpl-2.0.html
 *
 * Network:            false.
 * Update URI:         /----------/
 */

defined("ABSPATH") or die;

define( 'WEATHER_PLUGIN', __FILE__ );
define( 'WEATHER_PLUGIN_DIR', untrailingslashit( dirname( WEATHER_PLUGIN ) ) );
// TODO: <-- вынести в конфиг
define( 'WEATHER_API_KEY', "3e5caaf491b920c88b6d939eb9248551" );
// TODO: -->

if ( is_admin() ) {
	require_once WEATHER_PLUGIN_DIR . '/admin/admin.php';
}

register_activation_hook( __FILE__, 'weather_activate' );
add_shortcode( "weather" , "do_weather" );
add_action( 'wp_enqueue_scripts', 'plugin_styles');

function plugin_styles() {
	wp_register_style( 'weather-plugin', plugin_dir_url( __FILE__ ) . '/css/weather-plugin.css' );
	wp_enqueue_style( 'weather-plugin' );
	wp_register_script( 'weather-plugin-jq', 'https://code.jquery.com/jquery-3.7.1.min.js' );
	wp_enqueue_script( 'weather-plugin-jq' );
	wp_register_script( 'weather-plugin', plugin_dir_url( __FILE__ ) . '/js/weather-plugin.js' );
	wp_enqueue_script( 'weather-plugin' );
}


function weather_activate () {
	global $wpdb;
	$sql = "CREATE TABLE {$wpdb->prefix}weather_plugin ( 
	id int(11) NOT NULL AUTO_INCREMENT, 
	date_time DATETIME(6), 
	city varchar(255) DEFAULT NULL, 
	temp varchar(255) DEFAULT NULL,
	 UNIQUE KEY id (id)
	 );";
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option('weather_state', 'false');
}


function do_weather ($apiKey) {
	$plugin_state = filter_var( get_option('weather_state'), FILTER_VALIDATE_BOOLEAN );

	if (!$plugin_state)
		return null;

//	$responseRaw = wp_remote_get( "http://ip-api.com/php/". $_SERVER['REMOTE_ADDR']);

	$responseRaw = wp_remote_get( "https://ipwho.is/188.232.85.25");
	$rawBody = json_decode($responseRaw["body"]);

	if (empty($rawBody)) {
		return "<div class=\"weather-plugin\"> <div>Ошибка определения ГЕО</div></div>";
	}

	$latitude = $rawBody->latitude;
	$longitude = $rawBody->longitude;
	$responseRaw = wp_remote_get("https://api.openweathermap.org/data/2.5/weather?lat=" . $latitude . "&lon=" . $longitude . "&appid=" . WEATHER_API_KEY . "&units=metric&lang=ru");
	$rawBody = json_decode($responseRaw["body"]);

	if (empty($rawBody)) {
		return "<div class=\"weather-plugin\"> <div>Сервер погоды не доступен</div></div>";
	}

	$city = sanitize_text_field($rawBody->name);
	$temp = sanitize_text_field($rawBody->main->temp);
	$description = sanitize_text_field($rawBody->weather[0]->description);

	global $wpdb;
	$count = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}weather_plugin");

	if ($count == 5)
		$wpdb->query("DELETE FROM {$wpdb->prefix}weather_plugin order by id asc limit 1;");

	$wpdb->insert("{$wpdb->prefix}weather_plugin", ["date_time" => "NOW()", "city" => $city, "temp" => $temp]);

	return "<div class=\"weather-plugin\"><h3>{$city}</h3> <div>{$temp} {$description}</div></div>";
}

